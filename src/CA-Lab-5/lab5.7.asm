.data 0x10000000
word1: .word 0x89abcdef
.text
.globl main
main:
    lui		$t4, 0x1000		# $t4 <- 0x10000000
    lwl		$t0, 0($t4)		# $t0 <- Memory[0x10000000-0x10000000]
    lwl		$t1, 1($t4)		# $t1 <- Memory[0x10000000-0x10000001]
    lwl		$t2, 2($t4)		# $t2 <- Memory[0x10000000-0x10000002]
    lwl		$t3, 3($t4)		# $t3 <- Memory[0x10000000-0x10000003]
    jr      $ra

