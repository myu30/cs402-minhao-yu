.data 0x10000000
word1: .word 0x89abcdef
.text
.globl main
main:
    la		$t0, word1		# $t4 <- 0x10000000
    lwr		$t4, 0($t0)		# $t0 <- Memory[0x10000000-0x10000003]
    lwr		$t5, 1($t0)		# $t1 <- Memory[0x10000001-0x10000003]
    lwr		$t6, 2($t0)		# $t2 <- Memory[0x10000002-0x10000003]
    lwr		$t7, 3($t0)		# $t3 <- Memory[0x10000003-0x10000003]
    jr      $ra

