.data
msgInput: .asciiz "Please enter the integer number: "
msgResult: .asciiz "If bytes were layed in reverse order the number would be: "
user1: .word 0
.text
.globl main
main:
    addu $sp, $sp, -4
    sw $ra, 0($sp) # push $ra to stack

# guide user to input an integer
    li $v0, 4 # system call for print_str
    la $a0, msgInput # address of string to print
    syscall
# now get an integer from the user
    li $v0, 5 # system call for read_int
    syscall # the integer placed in $v0
    sw $v0, user1
    la $a0, user1
    jal Reverse_bytes # call Reverse_bytes

# print a message with the reversed word
    li $v0, 4 # system call for print_str
    la $a0, msgResult # address of string to print
    syscall
    li $v0, 1 # system call for print_int
    lw $a0, user1 # $a0 <- A(x,y)
    syscall

    lw $ra, 0($sp)
    addu $sp, $sp, 4 # pop $ra from stack
    jr $ra

Reverse_bytes:
    lb $t0, 0($a0)
    lb $t1, 1($a0)
    lb $t2, 2($a0)
    lb $t3, 3($a0)
    sb $t0, 3($a0)
    sb $t1, 2($a0)
    sb $t2, 1($a0)
    sb $t3, 0($a0)
    jr $ra