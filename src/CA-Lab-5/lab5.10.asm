.data 0x10000000
.align 0
ch1: .byte 'a'
word1: .word 0x89abcdef
ch2: .byte 'b'
word2: .word 0x0
.text
.globl main
main:
lui $t0, 0x1000 # $t0 <- 0x10000000
lwr $t1, 1($t0) # load right part to $t1
lwl $t1, 4($t0) # load left part to $t1
swr $t1, 6($t0) # save right part of word2
swl $t1, 9($t0) # save left part of word2
jr $ra
nop