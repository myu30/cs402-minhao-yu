// workerDB.c
// 
// Author: Minhao Yu
// Date: 11-04-2019
// 
// Please refer to workerDB.h for detailed description.

#include "workerDB.h"

int main(int argc, char* argv[]){
    if (argc!=2){
        // If number of running parameters is not 2,
        // print an error message and exit the program.
        printf(TEXT_ERROR_CALL);
        return 0;
    }
    if (load_database(argv[1])==-1){
        // If file not found, exit the program
        return 0;
    }
    while(1){
        // Get operation code from user and execute operation.
        print_menu();
        int op=get_input_operation();
        switch(op){
            case 1:
                print_database();
                break;
            case 2:
                lookup_by_id();
                break;
            case 3:
                lookup_by_last_name();
                break;
            case 4:
                input_employee();
                break;
            case 5:
                printf(TEXT_EXIT);
                return 0;
        }
    };
    return 0;
}

void print_database(){
    // Print header of table
    printf(TEXT_TEMPLETE_TABLE_TITLE,COL_NAME,COL_SALARY,COL_ID);
    // Print a split line
    print_split();
    // Print employee in the database one by one
    for (int i=0;i<number_employees;i++){
        print_employee(employees[i]);
    }
    // Print a split line
    print_split();
    // Print the number of employees in the database
    printf(TEXT_TEMPLETE_NUMBER_EMPLOYEES,number_employees);
}

void lookup_by_id(){
    // Get an id from the user
    t_int_id id=get_input_id();
    // Try to find a employee with the id in the database
    Employee *e=get_employee_by_id(id);
    if (e==NULL){
        // If employee not found, print error message.
        printf(TEXT_TEMPLETE_ID_NOT_FOUND,id);
    }else{
        // If employee found, print the employee.
        print_employee_within_table(e);
    }
}

void lookup_by_last_name(){
    // Get a last name string from user
    char* last_name=get_intput_last_name();
    // Try to find a employee with the last name in the database
    Employee *e=get_employee_by_last_name(last_name);
    if (e==NULL){
        // If employee not found, print error message.
        printf(TEXT_TEMPLETE_LAST_NAME_NOT_FOUND,last_name);
    }else{
        // If employee found, print the employee.
        print_employee_within_table(e);
    }
    // Free the space of the string
    free(last_name);
}

void input_employee(){
    // Get string of first name from the user
    char* first_name=get_intput_first_name();
    // Get string of last name from the user
    char* last_name=get_intput_last_name();
    // Get salary from the user
    t_int_salary salary=get_input_salary();
    // Print input name and salary
    printf(TEXT_TEMPLETE_CONFIRM_EMPLOYEE,first_name,last_name,salary);
    // Get comfirmation for inserting the new employee
    if (get_input_confirm()==1){
        // If user confirmed, insert the new employee into database
        // Allocate memory for the new employee
        Employee* e=malloc(sizeof (Employee));
        // Assign an unused id
        e->id=get_next_id();
        // Assign salary and name
        e->salary=salary;
        strcpy(e->first_name,first_name);
        strcpy(e->last_name,last_name);
        // Insert the new employee into database
        employees[number_employees]=e;
        ++number_employees;
    }
    // Free the space used by name
    free(first_name);
    free(last_name);
}

t_int_input get_input_int(t_int_input min, t_int_input max, char* guide_msg, char* error_msg_template){
    // Print guide message in the terminal
    printf(guide_msg);
    // Allocate space for input string
    char str[MAXINPUT];
    // Get input string from user
    gets(str);
    // Cast string to integer
    t_int_input ret=atoll(str);
    // If input integer is out of bound, guide the user to input again,
    // util the input value is between min and max.
    while(ret < min || ret > max){
        printf(error_msg_template,str);
        printf(guide_msg);
        gets(str);
        ret=atoll(str);
    }
    return ret;
}

int get_input_operation(){
    // Call get_input_int() to get operation code from the user.
    return (int)get_input_int(MIN_OP,MAX_OP,TEXT_ENTER_CHOICE,TEXT_TEMPLETE_WRONG_CODE);
}

t_int_id get_input_id(){
    // Call get_input_int() to get id number from the user.
    return get_input_int(MIN_ID,MAX_ID,TEXT_ENTER_ID,TEXT_TEMPLETE_WRONG_ID);
}

t_int_salary get_input_salary(){
    // Call get_input_int() to get salary number from the user.
    return get_input_int(MIN_SALARY,MAX_SALARY,TEXT_ENTER_SALARY,TEXT_TEMPLETE_WRONG_SALARY);
}

int get_input_confirm(){
    // Call get_input_int() to get confirmation code from the user.
    return (int)get_input_int(MIN_COMFIRM,MAX_CONFIRM,TEXT_ENTER_CONFIRM,TEXT_TEMPLETE_WRONG_CONFIRM);
}

char* get_input_str(char* guide_msg){
    // Print the guide message in the terminal
    printf(guide_msg);
    // Allocate space for input string
    char* str=malloc(sizeof (char)*MAXINPUT);
    // Get input string from user
    gets(str);
    return str;
}

char* get_intput_last_name(){
    // Call get_input_string() to get last name from the user.
    return get_input_str(TEXT_ENTER_LAST_NAME);
}

char* get_intput_first_name(){
    // Call get_input_string() to get first name from the user.
    return get_input_str(TEXT_ENTER_FIRST_NAME);
}

void print_menu(){
    printf(text_menu);
}

void print_employee(Employee *e){
    printf(TEXT_TEMPLETE_TABLE_ENTITY,e->first_name,e->last_name,e->salary,e->id);
}

void print_employee_within_table(Employee *e){
    printf(TEXT_TEMPLETE_TABLE_TITLE,COL_NAME,COL_SALARY,COL_ID);
    print_split();
    print_employee(e);
    print_split();
}

void print_split(){
    for (int i=0;i<NUM_TABLE_SPLIT;i++){
        printf(CHAR_TABLE_SPLIT);
    }
    printf("\n");
}

int load_database(char* path){
    if (open_file(path)==-1){
        // If error occurs when open file, such as file not found, print error message.
        printf(TEXT_TEMPLETE_FILE_NOT_FOUND,path);
        return -1;
    }
    Employee* e;
    char line[MAX_LINE];
    int error_count=0;
    int line_count=0;
    while(read_next_line(line)==0){
        line_count++;
        e=load_employee_from_line(line);
        if (e!=NULL && is_valid_employee(e)){
            // If current line is a valid emloyee entity,
            // insert into database.
            insert_employee(e);
        }else{
            // Otherwise, print an error message.
            free(e);
            error_count++;
            printf(TEXT_TEMPLETE_FILE_LINE_ERROR,line_count,line);
        }
    }
    // Print the number of lines which are invalid.
    if (error_count>0)
        printf(TEXT_TEMPLETE_FILE_LINE_ERROR_COUNT,error_count);
    // Print the number of employees successfully loaded
    printf(TEXT_TEMPLETE_NUMBER_LOADED,number_employees,path);
    return 0;
}

Employee* load_next_employee(){
    // Allocate space for the the next Employee
    Employee* e=malloc(sizeof(Employee));
    // Try to read id number from file. If failed, return NULL.
    if (read_int(&(e->id))==-1)
        return NULL;
    // Read first name string from file
    read_string(e->first_name);
    // Read last name string from file
    read_string(e->last_name);
    // Read salary number from file
    read_int(&(e->salary));
    return e;
}

Employee* load_employee_from_line(char* line){
    char s[MAX_LINE];
    strcpy(s,line);
    char *token;
    // Read id from line
    token = strtok(s, FILE_SEPARATOR);
    if (token==NULL) return NULL;
    t_int_id id=(t_int_id)atoll(token);
    // Read first name from line
    token = strtok(NULL, FILE_SEPARATOR);
    if (token==NULL) return NULL;
    char* first_name=token;
    // Read last name from line
    token = strtok(NULL, FILE_SEPARATOR);
    if (token==NULL) return NULL;
    char* last_name=token;
    // Read salary from line
    token = strtok(NULL, FILE_SEPARATOR);
    if (token==NULL) return NULL;
    t_int_salary salary=(t_int_salary)atoll(token);
    Employee* e=malloc(sizeof (Employee));
    e->id=id;
    e->salary=salary;
    strcpy(e->first_name,first_name);
    strcpy(e->last_name,last_name);
    return e;
}

int is_valid_employee(Employee *e){
    if (e->id > MAX_ID || e->id < MIN_ID)
        return 0;
    if (e->salary > MAX_SALARY || e->salary < MIN_SALARY)
        return 0;
    if (strlen(e->first_name)<1)
        return 0;
    if (strlen(e->last_name)<1)
        return 0;
    return 1;
}

void insert_employee(Employee* e){
    // Find the insert postion of new employee in the array
    int pos=0;
    while(pos<number_employees && e->id>employees[pos]->id){
        ++pos;
    }
    // Move employees with larger id backward by one positon.
    for (int i=number_employees-1;i>=pos;i--){
        employees[i+1]=employees[i];
    }
    // Insert the new employee
    employees[pos]=e;
    ++number_employees;
}

Employee* get_employee_by_id(t_int_id id){
    // This method uses biosearch to find the employee with a given id.
    int left=0;
    int right=number_employees-1;
    while(left<=right){
        int mid=(left+right)/2;
        if (employees[mid]->id==id){
            return employees[mid];
        } else if (employees[mid]->id>id){
            right=mid-1;
        } else {
            left=mid+1;
        }
    }
    return NULL;
}

Employee* get_employee_by_last_name(char* last_name){
    // This method uses linear search to find the employee with a given last name.
    for (int i=0;i<number_employees;i++){
        if (strcmp(employees[i]->last_name,last_name)==0){
            return employees[i];
        }
    }
    return NULL;
}

t_int_id get_next_id(){
    if (number_employees==0){
        // If database is empty, return the minimum valid id.
        return MIN_ID;
    } else {
        // Otherwise, return successor of maximum id in the database
        return employees[number_employees-1]->id+1;
    }
}