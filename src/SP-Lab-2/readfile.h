// readfile.c
// 
// Author: Minhao Yu
// Date: 11-04-2019
// 
// This library provides severl methods to simplify reading from file.


#include <stdio.h>

// Maximun number of characters in a single line.
#define MAX_LINE 1024

// Pointer to the current file
FILE* p_file;

// Open a file with reading mode.
// Return 1, if the file is successfully opened. -1 if the file cannot be opened.
int open_file(char* file_name);

// Close the current file.
// Return 0;
int close_file();

// Read a float number from the current file.
// Return 0 on success. -1 on EOF.
int read_float(float* f);

// Read a integer number from the current file.
// Return 0 on success. -1 on EOF.
int read_int(unsigned long* i);

// Read a string from the current file.
// Return 0 on success. -1 on EOF.
int read_string(char* s);

// Read a string from the current file.
// Return 0 on success. -1 on EOF.
int read_next_line(char* s);

// Return 1 if current position is the end of the file. 0, otherwise.
int read_is_end();