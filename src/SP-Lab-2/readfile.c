// readfile.c
// 
// Author: Minhao Yu
// Date: 11-04-2019
// 
// Please refer to readfile.h for detailed description.

#include "readfile.h"

int open_file(char* file_name){
    p_file=fopen(file_name,"r");
    if (p_file==NULL)
        return -1;
    return 0;
}

int close_file(){
    fclose(p_file);
    return 0;
}

int read_float(float* f){
    int ret=fscanf(p_file,"%f",f);
    if (ret==EOF)
        return -1;
    else
        return 0;
}

int read_int(unsigned long* i){
    int ret=fscanf(p_file,"%lu",i);
    if (ret==EOF)
        return -1;
    else
        return 0;
}

int read_string(char* s){
    int ret=fscanf(p_file,"%s",s);
    if (ret==EOF)
        return -1;
    else
        return 0;
}

int read_next_line(char* s){
    if (feof(p_file)){
        return -1;
    }else{
        fgets(s,MAX_LINE,p_file);
        return 0;
    }
}

int read_is_end(){
    return feof(p_file);
}