// workerDB.c
// 
// Author: Minhao Yu
// Date: 11-12-2019
// 
// Please refer to workerDB.h for detailed description.

#include "workerDB.h"

int main(int argc, char* argv[]){
    if (argc!=2){
        // If number of running parameters is not 2,
        // print an error message and exit the program.
        printf(TEXT_ERROR_CALL);
        return 0;
    }
    // The list which stores all the employees
    Node *employee_list=get_new_list();
    if (load_database(employee_list,argv[1])==-1){
        // If file not found, exit the program
        return 0;
    }
    while(1){
        // Get operation code from user and execute operation.
        print_menu();
        int op=get_input_operation();
        switch(op){
            case 1:
                print_database(employee_list);
                break;
            case 2:
                lookup_by_id(employee_list);
                break;
            case 3:
                lookup_by_last_name(employee_list);
                break;
            case 4:
                input_employee(employee_list);
                break;
            case 5:
                printf(TEXT_EXIT);
                return 0;
            case 6:
                remove_by_id(employee_list);
                break;
            case 7:
                update_by_id(employee_list);
                break;
            case 8:
                lookup_highest_salary(employee_list);
                break;
        }
    };
    return 0;
}

void print_database(Node *list){
    // Print header of table
    printf(TEXT_TEMPLETE_TABLE_TITLE,COL_NAME,COL_SALARY,COL_ID);
    // Print a split line
    print_split();
    // Print employee in the database one by one
    Node *node=list->next;
    while (node!=list){
        print_employee(node->employee);
        node=node->next;
    }
    // Print a split line
    print_split();
    // Print the number of employees in the database
    printf(TEXT_TEMPLETE_NUMBER_EMPLOYEES,size_of_list(list));
}

void lookup_by_id(Node *list){
    // Get an id from the user
    t_int_id id=get_input_id();
    // Try to find a employee with the id in the database
    Employee *e=get_employee_by_id(list,id);
    if (e==NULL){
        // If employee not found, print error message.
        printf(TEXT_TEMPLETE_ID_NOT_FOUND,id);
    }else{
        // If employee found, print the employee.
        print_employee_within_table(e);
    }
}

void lookup_by_last_name(Node *list){
    // Get a last name string from user
    char* last_name=get_intput_last_name();
    // Try to find all the employee with the last name in the database
    Node *match_list=get_new_list();
    get_employees_by_last_name(list,last_name,match_list);
    if (size_of_list(match_list)==0){
        // If employee not found, print error message.
        printf(TEXT_TEMPLETE_LAST_NAME_NOT_FOUND,last_name);
    }else{
        // If employee found, print the employee.
        print_database(match_list);
    }
    // Free the space of the list and string
    free_list(match_list);
    free(last_name);
}

void input_employee(Node *list){
    // Get string of first name from the user
    char* first_name=get_intput_first_name();
    // Get string of last name from the user
    char* last_name=get_intput_last_name();
    // Get salary from the user
    t_int_salary salary=get_input_salary();
    // Print input name and salary
    printf(TEXT_TEMPLETE_CONFIRM_INSERT_EMPLOYEE,first_name,last_name,salary);
    // Get comfirmation for inserting the new employee
    if (get_input_confirm()==1){
        // If user confirmed, insert the new employee into database
        // Allocate memory for the new employee
        Employee* e=malloc(sizeof (Employee));
        // Assign an unused id
        e->id=get_next_id(list);
        // Assign salary and name
        e->salary=salary;
        strcpy(e->first_name,first_name);
        strcpy(e->last_name,last_name);
        // Insert the new employee into database
        insert_to_list_back(list,e);
    }
    // Free the space used by name
    free(first_name);
    free(last_name);
}

void remove_by_id(Node *list){
    // Get an id from the user
    t_int_id id=get_input_id();
    // Try to find a employee with the id in the database
    Node *node=get_node_by_id(list,id);
    if (node==NULL){
        // If employee not found, print error message.
        printf(TEXT_TEMPLETE_ID_NOT_FOUND,id);
    }else{
        // If employee found,
        // print the employee.
        printf(TEXT_TEMPLETE_CONFIRM_REMOVE_EMPLOYEE,
        node->employee->first_name,
        node->employee->last_name,
        node->employee->salary);
        // Get comfirmation for removing the new employee
        if (get_input_confirm()==1){
            // If confirmed, remove from database
            remove_from_list(node);
            free(node->employee);
            free(node);
        }
    }
}

void update_by_id(Node *list){
    // Get an id from the user
    t_int_id id=get_input_id();
    // Try to find a employee with the id in the database
    Employee *emlopyee=get_employee_by_id(list,id);
    if (emlopyee==NULL){
        // If employee not found, print error message.
        printf(TEXT_TEMPLETE_ID_NOT_FOUND,id);
    }else{
        // If employee found,
        // print the employee.
        print_employee_within_table(emlopyee);
        printf(TEXT_INPUT_UPDATE);
        // Get string of first name from the user
        char* first_name=get_intput_first_name();
        // Get string of last name from the user
        char* last_name=get_intput_last_name();
        // Get salary from the user
        t_int_salary salary=get_input_salary();
        printf(TEXT_TEMPLETE_CONFIRM_UPDATE_EMPLOYEE,id,first_name,last_name,salary);
        // Get comfirmation for updating
        if (get_input_confirm()==1){
            // If confirmed, update employee in datebase
            emlopyee->salary=salary;
            strcpy(emlopyee->first_name,first_name);
            strcpy(emlopyee->last_name,last_name);
        }
        free(first_name);
        free(last_name);
    }
}

void lookup_highest_salary(Node *list){
    if (size_of_list(list)==0){
        printf(TEXT_LOOKUP_EMPTY_DB_EORROR);
        return;
    }
    int num=(int)get_input_int(1,size_of_list(list),TEXT_ENTER_NUMBER_HIGHTEST_SALARY,TEXT_TEMPLETE_WRONG_RANGE);
    Node *emps=get_new_list();
    get_highest_salary_employees(list,num,emps);
    print_database(emps);
    free_list(emps);
}

t_int_input get_input_int(t_int_input min, t_int_input max, char* guide_msg, char* error_msg_template){
    // Print guide message in the terminal
    printf(guide_msg);
    // Allocate space for input string
    char str[MAXINPUT];
    // Get input string from user
    gets(str);
    // Cast string to integer
    t_int_input ret=atoll(str);
    // If input integer is out of bound, guide the user to input again,
    // util the input value is between min and max.
    while(ret < min || ret > max){
        printf(error_msg_template,str,min,max);
        printf(guide_msg);
        gets(str);
        ret=atoll(str);
    }
    return ret;
}

int get_input_operation(){
    // Call get_input_int() to get operation code from the user.
    return (int)get_input_int(MIN_OP,MAX_OP,TEXT_ENTER_CHOICE,TEXT_TEMPLETE_WRONG_RANGE);
}

t_int_id get_input_id(){
    // Call get_input_int() to get id number from the user.
    return get_input_int(MIN_ID,MAX_ID,TEXT_ENTER_ID,TEXT_TEMPLETE_WRONG_RANGE);
}

t_int_salary get_input_salary(){
    // Call get_input_int() to get salary number from the user.
    return get_input_int(MIN_SALARY,MAX_SALARY,TEXT_ENTER_SALARY,TEXT_TEMPLETE_WRONG_RANGE);
}

int get_input_confirm(){
    // Call get_input_int() to get confirmation code from the user.
    return (int)get_input_int(MIN_COMFIRM,MAX_CONFIRM,TEXT_ENTER_CONFIRM,TEXT_TEMPLETE_WRONG_RANGE);
}

char* get_input_str(char* guide_msg){
    // Print the guide message in the terminal
    printf(guide_msg);
    // Allocate space for input string
    char* str=malloc(sizeof (char)*MAXINPUT);
    // Get input string from user
    gets(str);
    return str;
}

char* get_intput_last_name(){
    // Call get_input_string() to get last name from the user.
    return get_input_str(TEXT_ENTER_LAST_NAME);
}

char* get_intput_first_name(){
    // Call get_input_string() to get first name from the user.
    return get_input_str(TEXT_ENTER_FIRST_NAME);
}

void print_menu(){
    printf(text_menu);
}

void print_employee(Employee *e){
    printf(TEXT_TEMPLETE_TABLE_ENTITY,e->first_name,e->last_name,e->salary,e->id);
}

void print_employee_within_table(Employee *e){
    printf(TEXT_TEMPLETE_TABLE_TITLE,COL_NAME,COL_SALARY,COL_ID);
    print_split();
    print_employee(e);
    print_split();
}

void print_split(){
    for (int i=0;i<NUM_TABLE_SPLIT;i++){
        printf(CHAR_TABLE_SPLIT);
    }
    printf("\n");
}

int load_database(Node *list, char* path){
    if (open_file(path)==-1){
        // If error occurs when open file, such as file not found, print error message.
        printf(TEXT_TEMPLETE_FILE_NOT_FOUND,path);
        return -1;
    }
    Employee* e;
    char line[MAX_LINE];
    int error_count=0;
    int line_count=0;
    while(read_next_line(line)==0){
        line_count++;
        e=load_employee_from_line(line);
        if (e!=NULL && is_valid_employee(e)){
            // If current line is a valid emloyee entity,
            // insert into database.
            insert_to_list(list,e);
        }else{
            // Otherwise, print an error message.
            free(e);
            error_count++;
            printf(TEXT_TEMPLETE_FILE_LINE_ERROR,line_count,line);
        }
    }
    // Print the number of lines which are invalid.
    if (error_count>0)
        printf(TEXT_TEMPLETE_FILE_LINE_ERROR_COUNT,error_count);
    // Print the number of employees successfully loaded
    printf(TEXT_TEMPLETE_NUMBER_LOADED,size_of_list(list),path);
    return 0;
}

Employee* load_next_employee(){
    // Allocate space for the the next Employee
    Employee* e=malloc(sizeof(Employee));
    // Try to read id number from file. If failed, return NULL.
    if (read_int(&(e->id))==-1)
        return NULL;
    // Read first name string from file
    read_string(e->first_name);
    // Read last name string from file
    read_string(e->last_name);
    // Read salary number from file
    read_int(&(e->salary));
    return e;
}

Employee* load_employee_from_line(char* line){
    char s[MAX_LINE];
    strcpy(s,line);
    char *token;
    // Read id from line
    token = strtok(s, FILE_SEPARATOR);
    if (token==NULL) return NULL;
    t_int_id id=(t_int_id)atoll(token);
    // Read first name from line
    token = strtok(NULL, FILE_SEPARATOR);
    if (token==NULL) return NULL;
    char* first_name=token;
    // Read last name from line
    token = strtok(NULL, FILE_SEPARATOR);
    if (token==NULL) return NULL;
    char* last_name=token;
    // Read salary from line
    token = strtok(NULL, FILE_SEPARATOR);
    if (token==NULL) return NULL;
    t_int_salary salary=(t_int_salary)atoll(token);
    Employee* e=malloc(sizeof (Employee));
    e->id=id;
    e->salary=salary;
    strcpy(e->first_name,first_name);
    strcpy(e->last_name,last_name);
    return e;
}

int is_valid_employee(Employee *e){
    if (e->id > MAX_ID || e->id < MIN_ID)
        return 0;
    if (e->salary > MAX_SALARY || e->salary < MIN_SALARY)
        return 0;
    if (strlen(e->first_name)<1)
        return 0;
    if (strlen(e->last_name)<1)
        return 0;
    return 1;
}

Employee* get_employee_by_id(Node *list, t_int_id id){
    Node* node=get_node_by_id(list, id);
    if (node==NULL){
        return NULL;
    }else{
        return node->employee;
    }
}

void get_employees_by_last_name(Node *source, char* last_name, Node *target){
    // This method uses linear search to find the employee with a given last name.
    Node *node=source->next;
    while (node!=source){
        if (ignoreCaseComp(node->employee->last_name,last_name)==0){
            insert_to_list_back(target,node->employee);
        }
        node=node->next;
    }
}

void get_highest_salary_employees(Node *source, int num, Node *target){
    // Remain positions in the target list.
    for(int i=0;i<num;++i){
        insert_to_list_back(target,NULL);
    }
    // Pass throught the source list
    Node *node=source->next;
    while(node!=source){
        Employee* e=node->employee;
        Node *t=target->next;
        // Try to insert the current employee into the X largest List
        while(t!=target){
            if (t->employee==NULL){
                // If there is an empty position, insert the employee.
                t->employee=e;
                break;
            }else{
                // If this is not an empty postion, compare the salary
                if (e->salary>t->employee->salary){
                    // If current employee have higher salary,
                    // swap current one and the one in the list.
                    Employee *tem=t->employee;
                    t->employee=e;
                    e=tem;
                }
            }
            t=t->next;
        }
        node=node->next;
    }
}

t_int_id get_next_id(Node *list){
    if (size_of_list(list)==0){
        // If database is empty, return the minimum valid id.
        return MIN_ID;
    } else {
        // Otherwise, return successor of maximum id in the database
        return list->pre->employee->id+1;
    }
}

Node* get_new_list(){
    Node *header=malloc(sizeof(Node));
    header->employee=&HEADER_EMPLOYEE;
    header->next=header;
    header->pre=header;
    return header;
}

Node* get_node_by_id(Node *list,t_int_id id){
    Node *node=list->next;
    while (node!=list){
        if (node->employee->id==id)
            return node;
        node=node->next;
    }
    return NULL;
}

void insert_to_list(Node *list,Employee *emp){
    Node *node=list->next;
    // find the first employee with bigger id than emp
    while (node!=list && node->employee->id<emp->id){
        node=node->next;
    }
     insert_before_node(node,emp);
}


void insert_to_list_back(Node *list,Employee *emp){
    insert_before_node(list,emp);
}

void insert_before_node(Node *node, Employee *emp){
    // new node stores emp
    Node *new_node=malloc(sizeof(Node));
    new_node->employee=emp;
    // insert in front of the found employee
    new_node->pre=node->pre;
    new_node->next=node;
    node->pre->next=new_node;
    node->pre=new_node;
}

int size_of_list(Node *list){
    int size=0;
    Node *node=list->next;
    while (node!=list){
        ++size;
        node=node->next;
    }
    return size;
}

void free_list(Node *list){
    Node *node=list->next;
    Node *pre;
    while(node!=list){
        pre=node;
        node=node->next;
        free(pre);
    }
    free(list);
}

void remove_from_list(Node *node){
    node->pre->next=node->next;
    node->next->pre=node->pre;
}

int ignoreCaseComp (const char *str1, const char *str2){
    if (!str1 || !str2)
        return 1;
    for (;;++str1,++str2){
        if (*str1=='\0' && *str2=='\0')
        // If both strings end, return same.
            return 0;
        if (*str1=='\0' || *str2=='\0')
        // If one of strings ends, return different.
            return 1;
        if ((*str1 | 32) != (*str2 | 32))
        // If current letters don't match, return different.
            return 1;
  }
}