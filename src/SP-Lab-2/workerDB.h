// workerDB.h
// 
// Author: Minhao Yu
// Date: 11-12-2019
// 
// This program implement an employee database.
// It stores employee information in an array of employee structs, 
// sorted by employee ID value.
// It reads in employee data from an input file when it starts-up.
// After that, it prints out a menu of transaction options, 
// read in the user's selection, 
// perform the chosen operation on the employee database, 
// and repeat until told to quit. 

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "readfile.h"

// max length of name
#define MAXNAME 64

// max length of input from user
#define MAXINPUT 128

// file separators in inputfile
#define FILE_SEPARATOR " "

// text for menu
char text_menu[]=
"\nEmployee DB Menu:\n"
"----------------------------------\n"
"  (1) Print the Database\n"
"  (2) Lookup by ID\n"
"  (3) Lookup by Last Name\n"
"  (4) Add an Employee\n"
"  (5) Quit\n"
"  (6) Remove by ID\n"
"  (7) Update by ID\n"
"  (8) Lookup highest salary employees\n"
"----------------------------------\n";

// templetes for get operation code
#define MAX_OP 8
#define MIN_OP 1
#define TEXT_ENTER_CHOICE "Enter your choice: "
#define TEXT_TEMPLETE_WRONG_RANGE "Hey, \"%s\" is NOT between %lu and %lu, try again...\n"

// templetes for get employee's id
#define MAX_ID 999999
#define MIN_ID 100000
#define TEXT_ENTER_ID "Enter a 6 digit employee id: "

// templetes for get confirm
#define MAX_CONFIRM 1
#define MIN_COMFIRM 0
#define TEXT_ENTER_CONFIRM "Enter 1 for yes, 0 for no: "
#define TEXT_TEMPLETE_CONFIRM_INSERT_EMPLOYEE "Do you want to add the following employee to the DB?\n    %s %s, salary: %lu\n"
#define TEXT_TEMPLETE_CONFIRM_REMOVE_EMPLOYEE "Do you want to REMOVE the following employee from the DB?\n    %s %s, salary: %lu\n"

// templetes for get employee's salary
#define MAX_SALARY 150000
#define MIN_SALARY 30000
#define TEXT_ENTER_SALARY "Enter employee's salary (30000 to 150000): "

// templetes for get employee's name
#define TEXT_ENTER_FIRST_NAME "Enter employee's first name: "
#define TEXT_ENTER_LAST_NAME "Enter employee's last name: "
#define TEXT_TEMPLETE_WRONG_NAME "Hey, \"%s\" is NOT a valid name, try again...\n"

// templetes for look_up_by_id
#define TEXT_TEMPLETE_ID_NOT_FOUND "\nEmployee with id %lu not found in DB\n"

// templetes for look_up_by_last_name
#define TEXT_TEMPLETE_LAST_NAME_NOT_FOUND "Employee with last name %s not found in DB\n"

// templetes for update_by_id
#define TEXT_INPUT_UPDATE "Please enter Updated information below:\n"

// templetes for lookup_highest_salary
#define TEXT_ENTER_NUMBER_HIGHTEST_SALARY "Enter number of employees with hightest salary: "
#define TEXT_LOOKUP_EMPTY_DB_EORROR "There is no data in the database, please import employees before lookup.\n"
#define TEXT_TEMPLETE_CONFIRM_UPDATE_EMPLOYEE "Do you want to update employee with ID %lu with following information?\n    %s %s, salary: %lu\n"
// templete for table
#define COL_NAME "NAME"
#define COL_SALARY "SALARY"
#define COL_ID "ID"
#define TEXT_TEMPLETE_TABLE_TITLE "\n%-26s%10s%10s\n"
#define TEXT_TEMPLETE_TABLE_ENTITY "%-13s%-13s%10lu%10lu\n"
#define CHAR_TABLE_SPLIT "-"
#define NUM_TABLE_SPLIT 46

// other templetes for UI
#define TEXT_TEMPLETE_NUMBER_EMPLOYEES " Number of Employees (%d)\n"
#define TEXT_TEMPLETE_FILE_NOT_FOUND "File \"%s\" is not found. Please try again."
#define TEXT_EXIT " Goodbye!\n"
#define TEXT_TEMPLETE_NUMBER_LOADED "%d employees successfully loaded from file \"%s\".\n"
#define TEXT_ERROR_CALL "Parameter error. Please use \"./workerDB.exe path.to.data.file\" to run the program.\n"
#define TEXT_TEMPLETE_FILE_LINE_ERROR "%6d: %s"
#define TEXT_TEMPLETE_FILE_LINE_ERROR_COUNT "\n%d lines have been ignored because of invalid data or format error.\n"

// define integer type for employ's id
typedef unsigned long t_int_id;
// define integer type for employ's salary
typedef unsigned long t_int_salary;
// define integer type for input from user
typedef unsigned long t_int_input;

// Stucture for an employee
typedef struct{
    t_int_id id;
    t_int_salary salary;
    char first_name[MAXNAME];
    char last_name[MAXNAME];
} Employee;

// Double linked list node
typedef struct node_struct{
    Employee *employee;
    struct node_struct *next;
    struct node_struct *pre;
} Node;

// Default hearder element for the circled double linked list.
Employee HEADER_EMPLOYEE={
    .id=MIN_ID-1,
    .salary=0,
    .first_name="",
    .last_name=""
};

// methods defined for each operation in the menu

// This method prints a table contains each entity in the employees list.
void print_database(Node *list);

// This method:
// 1. gets an id nubmer from user;
// 2. tries to find the employee with the same id in the array;
// 3. prints the empolyee, if found. Otherwise, print an error message.
void lookup_by_id(Node *list);

// This method:
// 1. gets a last name string from user;
// 2. tries to find the employee with the same last name in the array;
// 3. prints the empolyee, if found. Otherwise, print an error message.
void lookup_by_last_name(Node *list);

// This method:
// 1. gets valid information about a new employee from the user;
// 2. inserts the new employee into the employees array.
void input_employee(Node *list);

// This method:
// 1. gets an id nubmer from user;
// 2. tries to delete the employee with the same id in the database;
void remove_by_id(Node *list);

// This method:
// 1. gets an id nubmer from user;
// 2. tries to find the employee with the same id in the database;
// 3. gets updated information from the user;
// 4. update the employee with input information 
void update_by_id(Node *list);

// This method:
// 1. gets an nubmer N from user;
// 2. print N employees with highest salaries in the database.
void lookup_highest_salary(Node *list);

// methods defined for getting information from the user.

// This method gets an integer between min and max from the user.
// It will guide the user to input again if the input value is out of bound.
// Reutrn the integer.
t_int_input get_input_int(t_int_input min, t_int_input max, char* guide_msg, char* error_msg_template);

// This method gets an operation code from the user.
// It will guide the user to input again if the operation code is invalid.
// Reutrn the operation code.
int get_input_operation();

// This method gets an id from the user.
// It will guide the user to input again if the id is invalid.
// Reutrn the id.
t_int_id get_input_id();

// This method gets a salary number from the user.
// It will guide the user to input again if the salary number is invalid.
// Reutrn the salary number.
t_int_salary get_input_salary();

// This method gets a confirmation code from the user.
// It will guide the user to input again if the confirmation code is invalid.
// Reutrn 1 if user confirm the operation. 0, otherwise.
int get_input_confirm();

// This method gets a string from the user.
// This method allocates MAXINPUT bytes space which must be manually released.
// Reutrn the string.
char* get_input_str(char* guide_msg);

// This method gets a last name string from the user.
// This method allocates MAXINPUT bytes space which must be manually released.
// Reutrn the string.
char* get_intput_last_name();

// This method gets a first name string from the user.
// This method allocates MAXINPUT bytes space which must be manually released.
// Reutrn the string.
char* get_intput_first_name();

// methods defined for output

// This method prints the menu of the application.
void print_menu();

// This method prints an employee.
void print_employee(Employee *e);

// This method prints an empolyee with table title.
void print_employee_within_table(Employee *e);

// This method prints a split line.
void print_split();

// methods defined for database operation

// This method loads employees' information from a file into the database.
// Return 0 if loading is successful. -1, if the file is not found.
int load_database(Node *list,char *path);

// This method loads the next employees from the file.
// This method allocates sizeof(Employee) bytes space which must be manually released.
// Return the next employee if loading is successful. NULL, otherwise.
Employee* load_next_employee();

// This method loads a employees from the given string.
// This method allocates sizeof(Employee) bytes space which must be manually released.
// Return the employee if loading is successful. NULL, otherwise.
Employee* load_employee_from_line(char* s);

// A valid employee should have:
// 1. MIN_ID <= id <= MAX_ID;
// 2. MIN_SALARY <= salary <= MAX_SALARY;
// 3. len(first_name) > 0;
// 4. len(last_name) > 0;
// Return 1 if the employee is valid. 0, Otherwise.
int is_valid_employee(Employee *e);

// Insert an employee into the database by ID order.
void insert_employee(Employee *e);

// Retrun an employee with the given id from the list.
// Returen NULL, if not found.
Employee* get_employee_by_id(Node *list,t_int_id id);

// This method finds employees with the given last name from the source list.
// and stores them in the target list.
void get_employees_by_last_name(Node *source, char* last_name, Node *target);

// This method finds X employees with highest salary from the source list.
// and stores them in the target list.
void get_highest_salary_employees(Node *source, int num, Node *target);

// This method gets a next unused id.
// Return the minium value of id, if the database is empty. Largest id in the database + 1, otherwise.
t_int_id get_next_id(Node *list);

// methods defined for list operation

// Return root node of a new circled double linked list.
// The header node stores HEADER_EMPLOYEE.
Node* get_new_list();

// Retrun an list node with the given id from the list.
// Returen NULL, if not found.
Node* get_node_by_id(Node *list,t_int_id id);

// Insert a new emloyee into a list sorted by id.
// The list remains sorted after the insert.
void insert_to_list(Node *list,Employee *emp);

// Insert a new emloyee into a the back of the list.
void insert_to_list_back(Node *list,Employee *emp);

// Insert a new emloyee before a given node in the list.
void insert_before_node(Node *node, Employee *emp);

// Return size of a list excluding header and trailer
int size_of_list(Node *list);

// Free a list.
// Referenced employees will NOT be freed.
void free_list(Node *list);

// Remove a node from the list it belongs.
void remove_from_list(Node *node);

// Return 0 if two strings are same (case insensitive). 1, otherwise.
// str1, str2 shall be composed of letters only.
int ignoreCaseComp (const char *str1, const char *str2);