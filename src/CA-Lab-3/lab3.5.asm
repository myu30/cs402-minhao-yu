.data 0x10010000
msgInput1: .asciiz "Please enter an integer number: "
msgInput2: .asciiz "Please enter another integer number: "
msgFar: .asciiz "Im far away."
msgNear: .asciiz "Im near by."
.text
.globl main

main:
    addu $s0, $ra, $0 # save $31 in $16
# guide user to input an integer
    li $v0, 4 # system call for print_str
    la $a0, msgInput1 # address of string to print
    syscall
# now get an integer from the user
    li $v0, 5 # system call for read_int
    syscall # the integer placed in $v0
    move $t0, $v0 # move the first number to $t0
# guide user to input an integer
    li $v0, 4 # system call for print_str
    la $a0, msgInput2 # address of string to print
    syscall
# now get an integer from the user
    li $v0, 5 # system call for read_int
    syscall # the integer placed in $v0
    move $t1, $v0 # move the first number to $t1

# condition
    bne $t0, $t1, Nearby
    j Far
Nearby:
# print "I'm near by"
    li $v0, 4 # system call for print_str
    la $a0, msgNear # address of string to print
    syscall
    # restore now the return address in $ra and return from main
    addu $ra, $0, $s0 # return address back in $31
    jr $ra # return from main



.text 0x00409000
Far:
# print "I'm far"
    li $v0, 4 # system call for print_str
    la $a0, msgFar # address of string to print
    syscall
    addu $ra, $0, $s0 # return address back in $31
    jr $ra # return from main