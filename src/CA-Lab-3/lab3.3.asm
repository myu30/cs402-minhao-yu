.data 0x10010000
# I don't have a SSN number. Use A number istead.
# My A number is A20446516

var1: .word 2 # First digit of my A number 2

.text

.globl main

main:
    lw $t0, var1 # load var1 to $t0
    addu $t1, $t0, $0 # $t1 <- $t0
    li $t2, 100  # $t2 <- 100
Loop:
    ble $t2, $t1, Exit # if $t2 <= $t1, jump to Exit
    addi $t0, $t0, 1 # $t0 <- $t0 + 1
    addi $t1, $t1, 1 # $t1 <- $t1 + 1
    j Loop
Exit:
    sw $t0, var1 # save $t0 to var1
    jr $ra # return from main  
