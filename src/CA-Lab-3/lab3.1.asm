.data 0x10010000
# I don't have a SSN number. Use A number istead.
# My A number is A20446516
var1: .word 2 # First digit of my A number 2
var2: .word 0 # Second digit of my A number 0
var3: .word -2019 # Minus year number -2019

.text

.globl main

main:

    lw $t0, var1 # load var1 to $t0
    lw $t1, var2 # load var2 to $t1
    bne $t0, $t1, Else  # if $t0 != $t1, jump to Else
# var1 == var2
    lw $t2, var3 # load var3 to $t2
    sw $t2, var1 # save $t2 to var1
    sw $t2, var2 # save $t2 to var2
    j Exit # jump to Exit
Else:
# var1 != var2
    sw $t1, var1 # save $t1 to var1
    sw $t0, var2 # save $to to var2
Exit:
    jr $ra # return from main  
