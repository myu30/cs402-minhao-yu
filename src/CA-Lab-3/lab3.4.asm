.data 0x10010000
# I don't have a SSN number. Use A number istead.
# My A number is A20446516

initial_value: .word 2 # First digit of my A number 2

array: .space 40 # a array of 10 words

.text

.globl main

main:
    lw $t0, initial_value # load initial_value to $t0
    move $t1, $0 # set $t0 to 0
    li $t2, 40 # load 40 to $t2
Loop:
    ble $t2, $t1, Exit # if $t2 <= $t1, jump to Exit
    sw $t0, array($t1) # save $t0 to M[array+$t1]
    addi $t0, $t0, 1 # $t0 <- $t0 + 1
    addi $t1, $t1, 4 # $t1 <- $t1 + 4
    j Loop # jump to Loop
Exit:
    jr $ra # return from main  
