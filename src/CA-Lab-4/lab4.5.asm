.data 0x10010000
msgInput: .asciiz "Please enter a none-negative integer number: "
msgError: .asciiz "You have input a negetive number.\n"
msgFactorial: .asciiz "Factorial: "

.text
.globl main

main:
# push $ra to stack
    addu $sp, $sp, -4
    sw $ra, 0($sp) # push $ra to stack
Input:
# guide user to input an integer
    li $v0, 4 # system call for print_str
    la $a0, msgInput # address of string to print
    syscall
# now get an integer from the user
    li $v0, 5 # system call for read_int
    syscall # the integer placed in $v0
    bgez $v0, Positive # if $v0 >= 0, jump to Positive
# print an error message
    li $v0, 4 # system call for print_str
    la $a0, msgError # address of string to print
    syscall
    j Input # jump to Input
Positive:
# call the factorial procedure
    move $a0, $v0 # move the input number to $a0
    jal Factorial # call Factorial
    move $t0, $v0 # $t0 <- return value
# print a message with the factorial
    li $v0, 4 # system call for print_str
    la $a0, msgFactorial # address of string to print
    syscall
    li $v0, 1 # system call for print_int
    move $a0, $t0 # $a0 <- factorial
    syscall
# pop $ra from stack
    lw $ra, 0($sp)
    addu $sp, $sp, 4 # pop $ra from stack
# return to caller
    jr $ra

Factorial:
    addu $sp, $sp, -4
    sw $ra, 0($sp) # push return address to stack

    beqz $a0, terminate # if $a0 == 0, jump to terminate
    addu $sp, $sp, -4
    sw $a0, 0($sp) # push $a0 to stack
    addi $a0, $a0, -1 # $a0 <- $a0 - 1
    jal Factorial # call Factorial
    lw $a0, 0($sp) # load $a0 from stack
    mul $v0, $v0, $a0 # $v0 <- $v0 * $a0
    lw $ra, 4($sp) # load return address from stack
    addu $sp, $sp, 8 # pop return address, n from stack
    jr $ra # jump to return address
terminate:
    li $v0, 1 # $v0 <- 1
    lw $ra, 0($sp) # load return address from stack
    addu $sp, $sp, 4 # pop return address from stack
    jr $ra # jump to return address

