.data 0x10010000
msgInput1: .asciiz "Please enter an integer number: "
msgInput2: .asciiz "Please enter another integer number: "
msgBigger: .asciiz "The larger number is: "
.text
.globl main

main:
# push $ra to stack
    addu $sp, $sp, -4
    sw $ra, 0($sp) # push $ra to stack
# guide user to input an integer
    li $v0, 4 # system call for print_str
    la $a0, msgInput1 # address of string to print
    syscall
# now get an integer from the user
    li $v0, 5 # system call for read_int
    syscall # the integer placed in $v0
    move $t0, $v0 # move the first number to $t0
# guide user to input an integer
    li $v0, 4 # system call for print_str
    la $a0, msgInput2 # address of string to print
    syscall
# now get an integer from the user
    li $v0, 5 # system call for read_int
    syscall # the integer placed in $v0
    move $t1, $v0 # move the first number to $t1
# call the Larger procedure
    addu $sp, $sp, -8
    sw $t0, 4($sp) # push $t0 to stack
    sw $t1, 0($sp) # push $t1 to stack
    jal Larger # jump to Larger
    move $t2, $v0 # $t2 <- return value (larger number)
    lw $t0, 4($sp) # load $t0 from stack
    lw $t1, 0($sp) # load $t1 from stack
    addu $sp, $sp, 8 # pop $t0, $t1
# print a message with the larger number
    li $v0, 4 # system call for print_str
    la $a0, msgBigger # address of string to print
    syscall
    li $v0, 1 # system call for print_int
    move $a0, $t2 # $a0 <- larger number
    syscall
# pop $ra from stack
    lw $ra, 0($sp)
    addu $sp, $sp, 4 # pop $ra from stack
# return to caller
    jr $ra

Larger:
    lw $t0, 4($sp) # load $t0 from stack
    lw $t1, 0($sp) # load $t1 from stack
    blt $t0, $t1, T1Larger # if $t0 < $t1, jump to T1Larger
    move $v0, $t0 # return value <- $a0
    jr $ra # return to main
T1Larger:
    move $v0, $t1 # return value <- $t1
    jr $ra # return to main

