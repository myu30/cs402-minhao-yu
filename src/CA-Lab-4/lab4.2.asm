.text
.globl main
main:
    addu $sp, $sp, -4
    sw $ra, 0($sp) # push $ra to stack
    jal test # call test
    nop
    lw $ra, 0($sp)
    addu $sp, $sp, 4 # pop $ra from stack
    jr $ra

test:
    nop
    jr $ra