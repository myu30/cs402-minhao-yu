.data 0x10010000
msgInput1: .asciiz "Please enter the first none-negative integer number: "
msgInput2: .asciiz "Please enter the second none-negative integer number: "
msgError: .asciiz "You have input a negetive number.\n"
msgResult: .asciiz "A(x,y)= "

.text
.globl main

main:
# push $ra to stack
    addu $sp, $sp, -4
    sw $ra, 0($sp) # push $ra to stack

Input1:
# guide user to input an integer
    li $v0, 4 # system call for print_str
    la $a0, msgInput1 # address of string to print
    syscall
# now get an integer from the user
    li $v0, 5 # system call for read_int
    syscall # the integer placed in $v0
    bgez $v0, Positive1 # if $v0 >= 0, jump to Positive1
# print an error message
    li $v0, 4 # system call for print_str
    la $a0, msgError # address of string to print
    syscall
    j Input1 # jump to Input1
Positive1:
    move $t0, $v0 # move the input number to $t0

Input2:
# guide user to input an integer
    li $v0, 4 # system call for print_str
    la $a0, msgInput2 # address of string to print
    syscall
# now get an integer from the user
    li $v0, 5 # system call for read_int
    syscall # the integer placed in $v0
    bgez $v0, Positive2 # if $v0 >= 0, jump to Positive2
# print an error message
    li $v0, 4 # system call for print_str
    la $a0, msgError # address of string to print
    syscall
    j Input2 # jump to Input2
Positive2:
    move $t1, $v0 # move the input number to $t1

# call the Ackermann procedure
    move $a0, $t0 # move the fisrt input number to $a0
    move $a1, $t1 # move the second input number to $a1
    jal Ackermann # call Ackermann
    move $t2, $v0 # move return value to $v0 
# print a message with the A(x,y)
    li $v0, 4 # system call for print_str
    la $a0, msgResult # address of string to print
    syscall
    li $v0, 1 # system call for print_int
    move $a0, $t2 # $a0 <- A(x,y)
    syscall
# pop $ra from stack
    lw $ra, 0($sp)
    addu $sp, $sp, 4 # pop $ra from stack
# return to caller
    jr $ra

Ackermann:
    addu $sp, $sp, -4
    sw $ra, 0($sp) # push return address to stack

    beqz $a0, XE0 # if x == 0, jump to XE0
    addu $sp, $sp, -8
    sw $a0, 4($sp) # push x to stack
    sw $a1, 0($sp) # push y to stack
    beqz $a1, YE0 # if y == 0, jump to YE0

    add $a1, $a1, -1 # $a1 <- y - 1
    jal Ackermann # call A(x,y-1)
    move $a1, $v0 # $a1 <- A(x,y-1)
    add $a0, $a0, -1 # $a0 <- x-1
    jal Ackermann # call A(x-1,A(x,y-1))
    lw $a0, 4($sp) # load x from stack
    lw $a1, 0($sp) # load y from stack
    lw $ra, 8($sp) # load return address from stack
    addu $sp, $sp, 12 # pop y, x, return address from stack
    jr $ra # jump to return address
XE0:
# if x == 0
    addi $v0, $a1, 1 # return value <- y + 1
    lw $ra, 0($sp)
    addu $sp, $sp, 4 # pop return address from stack
    jr $ra # jump to return address
YE0:
# if y == 0
    add $a0, $a0, -1 # $a0 <- x - 1
    li $a1, 1 # $a1 <- 1
    jal Ackermann # call A(x-1,1)
    lw $a0, 4($sp) # load x from stack
    lw $a1, 0($sp) # load y from stack
    lw $ra, 8($sp) # load return address from stack
    addu $sp, $sp, 12 # pop y, x, return address from stack
    jr $ra # jump to return address

