// basicstats.h
// 
// Author: Minhao Yu
// Date: 11-19-2019
// 
// This program reads data from a file and computes some basic statistical analysis measures for the data.
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Default capacity of a new double array
#define DEFAULT_CAPACITY 20

// Multiple factor when a double array try to expand.
#define MULTIPLE_FACTOR 2

// The maximum number of characters in a single line from the input file.
#define MAX_LINE 128

// Text templates for output.

#define TEXT_ERROR_CALL "Parameter error. Please use \"./basicstats path.to.data.file\" to run the program.\n"
#define TEXT_FILE_NOT_FOUND "File \"%s\" is not found. Please try again."
#define TEXT_HEAD "\033[32mResults\033[0m:\n--------\n"
#define TEXT_NUMBER "\033[32mNum\033[0m values:\033[31m%14i\n"
#define TEXT_MEAN "\033[0m      mean:\033[31m%14.3f\n"
#define TEXT_MEDIAN "\033[0m    median:\033[31m%14.3f\n"
#define TEXT_STDDEV "\033[0m    stddev:\033[31m%14.3f\n"
#define TEXT_UNUSED_SPACE "\033[32mUnused\033[0m array capacity: \033[31m%d\033[0m\n"

// Methods defined for basicstats

// Read data from file.
double *read_data_from_file(double *list,int *capacity, int *size, char* file_name);

// quick sort a list
void quick_sort(double *list, int size);

// help method for quick_sort
void quick_sort_recursive(double *list,int left, int right);

// Return mean of a list. Return 0, if size is 0.
double mean_of_list(double *list, int size);

// Return median of a sorted list. Return 0, if size is 0.
double median_of_list(double *list, int size);

// Return standard deviation of a list. Return 0, if size is 0.
double stddev_of_list(double *list, int size, double mean);

// Methods defined for ArrayList

// This method allocates a new double array with size of DEFAULT_CAPACITY.
// *capacity <- DEFAULT_CAPACITY;
// *size <- 0.
// Return the new array.
double *get_new_list(double *list, int *capacity,int *size);

// Add a new element to a list.
// Return the list after adding.
double *add_to_list(double *list, int *capacity,int *size,double new_element);