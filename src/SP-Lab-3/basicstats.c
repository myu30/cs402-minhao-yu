// basicstats.c
// 
// Author: Minhao Yu
// Date: 11-19-2019
// 
// Please refer to basicstats.h for detailed description.
#include "basicstats.h"

int main(int argc, char* argv[]){
    if (argc!=2){
        // If number of running parameters is not 2,
        // print an error message and exit the program.
        printf(TEXT_ERROR_CALL);
        return 0;
    }
    // A double array storing input values.
    double *list;
    // Capacity of the list
    int capacity;
    // Current size of the list
    int size;
    // try to read from file
    list=read_data_from_file(list,&capacity,&size,argv[1]);
    // if read failed, return
    if (list==NULL)
        return 0;
    // sort the list
    quick_sort(list,size);
    // calculate stats result
    double mean=mean_of_list(list,size);
    double median=median_of_list(list,size);
    double stddev=stddev_of_list(list,size,mean);
    // print the result
    printf(TEXT_HEAD);
    printf(TEXT_NUMBER,size);
    printf(TEXT_MEAN,mean);
    printf(TEXT_MEDIAN,median);
    printf(TEXT_STDDEV,stddev);
    printf(TEXT_UNUSED_SPACE,capacity-size);
    free(list);
    return 0;
}

double *read_data_from_file(double *list,int *capacity, int *size, char* file_name){
    // Pointer to input file steam
    FILE *p_file;
    // Try to open file
    p_file=fopen(file_name,"r");
    if (p_file==NULL){
        // If error occurs when open file, such as file not found, print error message.
        printf(TEXT_FILE_NOT_FOUND,file_name);
        return NULL;
    }
    // Initiate the list
    list=get_new_list(list,capacity,size);
    // input buffer for reading string which is not a float number
    char str[MAX_LINE];
    // input float number
    double num;
    while(!feof(p_file)){
        int ret=fscanf(p_file,"%lf",&num);
        if (ret==1){
            // If succeed in reading float number, insert to the list.
            list=add_to_list(list,capacity,size,num);
        }else{
            // If not, ignore the next token.
            fscanf(p_file,"%s",str);
        }
    }
    return list;
}

void quick_sort(double *list, int size){
    quick_sort_recursive(list,0,size-1);
}

void quick_sort_recursive(double *list,int left, int right){
    if (left>=right)
        return;
    double tem=list[left];
    int l=left;
    int r=right;
    while (l<r){
        while(l<r && list[r]>=tem) --r;
        if (l<r) list[l]=list[r];
        while (l<r && list[l]<=tem) ++l;
        if (l<r) list[r]=list[l];
    }
    list[l]=tem;
    quick_sort_recursive(list,left,l-1);
    quick_sort_recursive(list,l+1,right);
}

double mean_of_list(double *list,int size){
    if (size==0)
        return 0;
    double sum=0;
    for (int i=0;i<size;++i){
        sum+=list[i];
    }
    return sum/size;
}

double median_of_list(double *list,int size){
    if (size==0)
        return 0;
    if (size % 2 ==0){
        return (list[size/2]+list[size/2-1])/2;
    }else{
        return (list[size/2]);
    }
}

double stddev_of_list(double *list, int size, double mean){
    if (size==0)
        return 0;
    double sum=0;
    for (int i=0;i<size;++i){
        sum+=pow(list[i]-mean,2);
    }
    return sqrt(sum/size);
}

double *get_new_list(double *list, int *capacity,int *size){
    list=malloc(sizeof(double) * DEFAULT_CAPACITY);
    *capacity=DEFAULT_CAPACITY;
    *size=0;
    return list;
}

double *add_to_list(double *list, int *capacity,int *size,double new_element){
    // If the array is full, expand the array.
    if (*size==*capacity){
        //allocate a new list with size of MULTIPLE_FACTOR times
        (*capacity)*=MULTIPLE_FACTOR;
        double *new_list=malloc(sizeof(double) * (*capacity));
        // copy value from old list to new list
        for (int i=0;i<(*size);i++)
            new_list[i]=list[i];
        // free space of old list
        free(list);
        list=new_list;
    }
    
    // insert the new element
    list[*size]=new_element;
    (*size)++;

    return list;
}