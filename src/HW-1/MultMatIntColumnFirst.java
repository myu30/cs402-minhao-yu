package hw01;

import java.util.Random;

/**
 * This is program which measures the time a computer cost to multiple two matrixes.
 * @author Minhao Yu
 */
public class MultMatIntColumnFirst {
	// keep random seed fixed 
	public static Random random=new Random(12345);
	
	// number of lines in the first matrix
	public static int aMatLineNum=600;
	
	// number of columns in the first matrix, same as number of lines in the second matrix
	public static int aMatColNum=1000;
	
	// number of columns in the second matrix
	public static int bMatColNum=1400;
	
	// max value in the matrix when randomly initiate it
	public static int maxValue=30000;
	
	// number of running rounds to get a average value
	public static int numRound=10;
	
	/**
	 * This is the main method which makes use of randomIntMat() method and mult() method.
	 * It will run several turns. Each turn,it randomizes two matrix, then measures the time used to call mult().
	 * At last, it will print the average time cost.
	 * @param args Unused.
	 */
	public static void main(String[] args) { 
		System.out.println("Running with integer and column first");
        System.out.println(String.format("Multiplies a %sx%s integer matric with a %sx%s integer matric", aMatLineNum,aMatColNum,aMatColNum,bMatColNum));
        long sum=0;
        for (int i=0;i<numRound;i++) {
        	int[][] aMat=randomMat(aMatLineNum,aMatColNum,maxValue);
            int[][] bMat=randomMat(aMatColNum,bMatColNum,maxValue);
	        long startTime=System.currentTimeMillis();
	        mult(aMat,bMat);
	        long endTime=System.currentTimeMillis();
	        long timeCost=endTime-startTime;
	        sum+=timeCost;
	        System.out.println(String.format("Round %s Time cost: %s ms",i,timeCost));
	    }
        System.out.println(String.format("Average time cost: %.2f ms",1d*sum/numRound));
    }

    /**
     * This method multiplies two matrix and return the result
     * @param a int[][] The first matrix
     * @param b int[][] The second matrix
     * @return int[][] The result matrix after multiple.
     */
    public static int[][] mult(int[][] a,int[][] b){
    	assert(a[0].length==b.length);
        int[][] mat=new int[a.length][b[0].length];
        for (int j=0;j<b[0].length;j++)
        	for (int i=0;i<a.length;i++){
                for (int k=0;k<b.length;k++){
                	mat[i][j]+=a[i][k]*b[k][j];
                }
            }
        return mat;
    }
    
    /**
     * This method generate a randomized matrix
     * @param numL integer Number of lines
     * @param numC integer Number of columns
     * @param maxValue integer the maximum value in the matrix
     * @return int[][] a randomized matrix with given number of lines and number of columns
     */
    public static int[][] randomMat(int numL,int numC, int maxValue){
    	int[][] mat=new int[numL][numC];
    	for (int i=0;i<numL;i++)
            for (int j=0;j<numC;j++){
            	mat[i][j]=random.nextInt(maxValue);
            }
    	return mat;
    }

    /**
     * This method print a 2D integer array in the terminal.
     * @param arr int[][] The array to be printed.
     */
    public static void printIntArray(int[][] arr){
		for (int i=0;i<arr.length;i++){
            for (int j=0;j<arr[0].length;j++){
                System.out.print(""+arr[i][j]+" ");
            }
            System.out.println();
        }
    }
}