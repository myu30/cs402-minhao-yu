.data 0x10010000
# My A number is A20446516
# var1 is a word (32 bit) with initial value
var1: .word 0x6
# var2 is a word (32 bit) with initial value
var2: .word 0x5
# var3 is a word (32 bit) with initial value
var3: .word 0x1
# var4 is a word (32 bit) with initial value
var4: .word 0x6
# first is a byte (8 bit) with initial value
first: .byte 'm'
# first is a byte (8 bit) with initial value
last: .byte 'y'

.text

.globl main

main:
# swap var1 with var4
lw $t0, var1 # load var1 to $t0
lw $t1, var4 # load var4 to $t1
sw $t0, var4 # save $t0 to var4
sw $t1, var1 # save $t1 to var1
# swap var2 with var3
lw $t0, var2 # load var2 to $t0
lw $t1, var3 # load var3 to $t1
sw $t0, var3 # save $t0 to var3
sw $t1, var2 # save $t1 to var2

jr $ra # return from main
