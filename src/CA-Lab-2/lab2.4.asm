.data 0x10010000
# My A number is A20446516
# var1 is a word (32 bit) with initial value
var1: .word 20
# var2 is a word (32 bit) with initial value
var2: .word 44

# ext1 is a word (32 bit)
.extern ext1 4
# ext2 is a word (32 bit)
.extern ext2 4

.text

.globl main

main:
lw $t0, var1 # load var1 to $t0
lw $t1, var2 # load var2 to $t1
sw $t0, ext2 # save $t0 to ext2
sw $t1, ext1 # save $t1 to ext1
  
jr $ra # return from main