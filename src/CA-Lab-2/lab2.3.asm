.data 0x10010000
# My A number is A20446516
# var1 is a word (32 bit) with initial value
var1: .word 0x6
# var2 is a word (32 bit) with initial value
var2: .word 0x5
# var3 is a word (32 bit) with initial value
var3: .word 0x1
# var4 is a word (32 bit) with initial value
var4: .word 0x6
# first is a byte (8 bit) with initial value
first: .byte 'm'
# first is a byte (8 bit) with initial value
last: .byte 'y'

.text

.globl main

main:

lui $8, 4097 # load 0x10010000 to $t0
lw $9, 0($8)   # load var1 to $t1
lw $10, 12($8)  # load var4 to $t2 
sw $9, 12($8) # save $t1 to var4 
sw $10, 0($8)  # save $t2 to var1 
lw $9, 4($8)  # load var2 to $t1 
lw $10, 8($8)  # load var3 to $t2 
sw $9, 8($8)  # save $t1 to var3 
sw $10, 4($8) # save $t2 to var2 

jr $ra # return from main  
addu $0,$0,$0 # nop