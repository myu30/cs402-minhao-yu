# CS402-Minhao-Yu
* [SP-Lab-1](#SP-Lab-1)
* [SP-Lab-2](#SP-Lab-2)
* [SP-Lab-3](#SP-Lab-3)

# SP-Lab-3
This program reads data from a file and computes some basic statistical analysis measures for the data.
The output looks like:
```shell
Results:
--------
Num values:            30
      mean:      2035.600
    median:      1956.000
    stddev:      1496.153
Unused array capacity: 10
```
## Compile & Run

Enter the directory
```shell
cd src/SP-Lab-3
```

To compile
```shell
gcc basicstats.c -o basicstats
```

To run with small test data
```shell
./basicstats small.txt
```

To run with large test data
```shell
./basicstats large.txt
```

To run with your own data
```shell
./basicstats path_to_your_data_file
```

# SP-Lab-2
This lab exapands the functionality of the employee database program developed in [SP-Lab-1](#SP-Lab-1), by adding the following features:
* Remove an employee
* Update an employee's information
* Print the M employees with the highest salaries
* Find all employees with matching last name


Now, the menu looks like:
```shell
Employee DB Menu:
----------------------------------
  (1) Print the Database
  (2) Lookup by ID
  (3) Lookup by Last Name
  (4) Add an Employee
  (5) Quit
  (6) Remove by ID
  (7) Update by ID
  (8) Lookup highest salary employees
----------------------------------
Enter your choice: 
```
## Compile & Run

Enter the directory
```shell
cd src/SP-Lab-2
```

To compile
```shell
gcc workerDB.c readfile.c -o workerDB
```

To run with test data
```shell
./workerDB small.txt
```

To run with your own data
```shell
./workerDB path_to_your_data_file
```

# SP-Lab-1
This lab implements an employee database program in C. The program will read in employee data from an input file when it starts-up. The program will guide the user to operate on the employee database.
```shell
Employee DB Menu:
----------------------------------
  (1) Print the Database
  (2) Lookup by ID
  (3) Lookup by Last Name
  (4) Add an Employee
  (5) Quit
----------------------------------
Enter your choice: 
```
## Compile & Run

Enter the directory
```shell
cd src/SP-Lab-1
```

To compile
```shell
gcc workerDB.c readfile.c -o workerDB
```

To run with test data
```shell
./workerDB small.txt
```

To run with your own data
```shell
./workerDB path_to_your_data_file
```